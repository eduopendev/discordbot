package edu.crabdev.bots.listeners;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class ListenerAdapterImpl extends ListenerAdapter {

    private static final Logger LOG = LogManager.getLogger(ListenerAdapterImpl.class);

    @Override
    public void onReady(ReadyEvent event) {
        LOG.info("API is ready!");
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        //Event specific information
        User author = event.getAuthor();                //The user that sent the message
        Message message = event.getMessage();           //The message that was received.
        MessageChannel channel = event.getChannel();    //This is the MessageChannel that the message was sent to.
        //  This could be a TextChannel, PrivateChannel, or Group!


        String msg = message.getContentDisplay();              //This returns a human readable version of the Message. Similar to
        // what you would see in the client.

        String responseMessage;

        if(channel.getType().equals(ChannelType.PRIVATE)) {
            responseMessage = makePrivateResponse(msg, author);
        } else {
            responseMessage = makePublicResponse(msg);
        }

        if(!author.isBot() && responseMessage != null) {
            channel.sendMessage(responseMessage).queue();
        }

        //LOG.info("сообщение: " + msg + ", автор:  " + author.getName() + ", канал: " + channel.getName());
    }

    private String makePublicResponse(String msg) {
        String responseMessage = null;
        if(msg.toLowerCase().equals("бот")) {
            responseMessage = "кто-то сказал бот? Аз есьм бот";
        }
        return responseMessage;
    }

    private String makePrivateResponse(String msg, User author) {
        String responseMessage;
        if (msg.toLowerCase().equals("привет")) {
            responseMessage = String.format("Привет и тебе, товарищ %s", author);
            //} else if(msg.toLowerCase().matches("время")) {
        } else if(Pattern.compile("(время)").matcher(msg.toLowerCase()).find()) {
            Date now = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("мое текущее время hh часов mm минут");
            responseMessage = formatForDateNow.format(now);
        } else {
            responseMessage = "ничего не знаю, кроме слова \"привет\", еще могу подсказать текущее \"время\"";
        }
        return responseMessage;
    }
}
